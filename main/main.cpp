#include <stdio.h>
#include <iostream>
#include <cstring>

// wifi header
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include <lwip/sockets.h>

// mqtt
#include "mqtt_client.hpp"
#include "mqtt_network_interface.hpp"

// other
#include "time_macros.hpp"
/* The wifi_credentials.hpp header declares 
 * static const char WIFI_SSID[]
 * static const char WIFI_PASS[]
 * which are used for the WIFI connection.*/
#include "wifi_credentials.hpp"

static const char MQTT_HOST[] = "192.168.178.21";

void test_function(void* arg);
void long_term_test();
void send_qos_packet(uint8_t qos);
void connect_to_broker();
void disconnect_from_broker();

// ############################################################################################
class mqtt_client_final : public mqtt::mqtt_client<mqtt::mqtt_network_interface, 10>
{
public:
	mqtt_client_final() : mqtt_client_final("") {};
	mqtt_client_final(const std::string& client_id) : mqtt::mqtt_client<mqtt::mqtt_network_interface, 10>(client_id) {};
	
	void onPublishReceived(const mqtt::publish_packet& obj)
	{
		printf("Received message: \n");
		// static_cast<mqtt::field>(obj).print();

		mqtt::message recv_msg = obj.decode();
		std::string str_msg    = recv_msg.to_string();
		std::cout << "  TOPIC: " << recv_msg.topic << std::endl;
		std::cout << "MESSAGE: " << str_msg << std::endl;


		std::size_t found = str_msg.find("disconnect");
		if (found!=std::string::npos)
			disconnect();
			// disconnect_from_broker();
	}
};

static mqtt_client_final client("ESP32_with_MQTTpp");

// ############################################################################################


// ############################################################################################
// 	WIFI Setup
// ############################################################################################

esp_err_t event_handler(void *ctx, system_event_t *event)
{
	switch(event->event_id) 
	{
		case SYSTEM_EVENT_STA_START:
			ESP_LOGI("WIFI", "SYSTEM_EVENT_STA_START");
			ESP_ERROR_CHECK(esp_wifi_connect());
			break;
		case SYSTEM_EVENT_STA_GOT_IP:
			ESP_LOGI("WIFI", "SYSTEM_EVENT_STA_GOT_IP");
			ESP_LOGI("WIFI", "got ip:%s\n",
			ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));

			// ******************************************************************************
			xTaskCreate(test_function, "test_function", 4048, nullptr, 10, nullptr);
			// ******************************************************************************

			break;
		case SYSTEM_EVENT_STA_DISCONNECTED:
			ESP_LOGI("WIFI", "SYSTEM_EVENT_STA_DISCONNECTED");

			// ******************************************************************************
			// client.disconnect();
			// ******************************************************************************

			ESP_ERROR_CHECK(esp_wifi_connect());
			break;
		default:
			break;
	}
	return ESP_OK;
}

// --------------------------------------------------------------------------------------

void setupWIFI(std::string ssid, std::string pwd)
{
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES) 
	{
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK( ret );

	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	wifi_config_t wifi_config = { };
	wifi_config.sta.bssid_set = false;
	memcpy(wifi_config.sta.ssid, ssid.c_str(), ssid.size());
	memcpy(wifi_config.sta.password, pwd.c_str(), pwd.size());

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());
}

// ############################################################################################
// 	Utilities
// ############################################################################################
static const uint32_t 	max_iterations 			= 100000;	// [-]
static const uint32_t 	packet_size 			= 15;		// [byte]
static const uint32_t 	keep_alive_after_test 	= 21000;	// [ms]
static const float 		message_frequence 		= 100.0;	// [Hz]
static char 			str_message[16];

void connect_to_broker()
{
	printf("[%015lld]\tConnecting to broker\n", NOW_MS());
	try
	{
		client.setHost(MQTT_HOST, 1883);
		client.setKeepAlive(10);
		client.connect();
		printf("[%015lld]\tConnected! Waiting 5 seconds\n", NOW_MS());
		vTaskDelay(5000/portTICK_PERIOD_MS);
	
		printf("[%015lld]\tSubscribing to test topic.\n", NOW_MS());
		// mqtt::topic_list_t topics(1);
		// topics[0] = std::make_pair<std::string, uint8_t>("/mqttpp/tests", 0);
		client.subscribe_send(mqtt::topic_list_t(1, std::make_pair("/mqttpp/tests/#", 0)));
	
		printf("[%015lld]\tWaiting 5 seconds\n", NOW_MS());
		vTaskDelay(5000/portTICK_PERIOD_MS);
	}
	catch(mqtt::mqtt_protocol_error& e)
	{
		std::cout << "MQTT exception occurred: " << e.what() << std::endl;
	}
	catch(network::network_error& e)
	{
		std::cout << "Socket exception occurred: " << e.what() << std::endl;
		esp_restart();
	}
	catch(...)
	{
		std::cout << "Unhandled exception occurred: " << std::endl;
	}
}

// --------------------------------------------------------------------------------------

void disconnect_from_broker()
{
	printf("[%015lld]\tDisconnecting to broker\n", NOW_MS());
	try
	{
		client.disconnect();
	}
	catch(mqtt::mqtt_protocol_error& e)
	{
		std::cout << "MQTT exception occurred: " << e.what() << std::endl;
	}
	catch(network::network_error& e)
	{
		std::cout << "Socket exception occurred: " << e.what() << std::endl;
		esp_restart();
	}
	catch(...)
	{
		std::cout << "Unhandled exception occurred: " << std::endl;
	}
}

// --------------------------------------------------------------------------------------

void test_function(void* arg)
{
	connect_to_broker();

	send_qos_packet(0);
	// for(auto it = 0; it < 100; ++it)
	while(true)
	{
		vTaskDelay(2000/portTICK_PERIOD_MS);
		send_qos_packet(0);
	}
		
	vTaskDelay(500/portTICK_PERIOD_MS);
	send_qos_packet(2);

	vTaskDelay(5000/portTICK_PERIOD_MS);
	disconnect_from_broker();
}

// --------------------------------------------------------------------------------------

void send_qos_packet(uint8_t qos)
{
	// printf("[%015lld]\tRunning 'QoS Packet' test with %d bytes and QoS=%d\n", NOW_MS(), packet_size, qos);

	mqtt::message msg(packet_size);
	msg.qos = qos;
	msg.topic = "/D/";

	sprintf(str_message, "%015lld", NOW_MS());
	for(auto it = 0; it < packet_size; ++it)
		msg[it] = str_message[it];

	try
	{client.publish(msg); }
	catch(const mqtt::mqtt_protocol_error& e)
	{
		std::cout << "MQTT exception occurred: " << e.what() << std::endl;
		vTaskDelay(1000/portTICK_PERIOD_MS);	// give the broker time to ACK all packets
	}
	catch(const network::network_error& e)
	{
		std::cout << "Socket exception occurred: " << e.what() << std::endl;
	}
	catch(...)
	{
		std::cout << "Unhandled exception occurred: " << std::endl;
	}
}

// --------------------------------------------------------------------------------------

void print_header()
{
	std::cout << "Compiler Version: " << __VERSION__ << std::endl;
	std::cout << " ESP-IDF Version: " << esp_get_idf_version() << std::endl;
	std::cout << std::endl;
	
}

// ############################################################################################
// 	MAIN
// ############################################################################################
extern "C" void app_main()
{
	print_header();

	printf("[%015lld]\tWelcome\n", NOW_MS());

	setupWIFI(std::string(WIFI_SSID), std::string(WIFI_PASS));

	printf("[%015lld]\tSetup complete\n",  NOW_MS());
}