COMPONENT_ADD_INCLUDEDIRS := ./protocol/inc ./client/inc
COMPONENT_SRCDIRS := ./protocol/src ./client/src
CXXFLAGS += -std=c++14 -fexceptions
CPPFLAGS += -std=c++14 -fexceptions