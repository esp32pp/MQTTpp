#pragma once

#include "mqtt_base.hpp"
#include "id_var_header.hpp"

namespace mqtt
{
	// *********************************************************************************
	// 	PUBACK
	// *********************************************************************************

	typedef id_var_header puback_var_header;

	// ----------------------------------------------------------------------------------------

	class puback_packet : public fixed_header
	{
	public:
		puback_packet(){ set(); }	
		puback_packet(uint16_t packet_id) { set(packet_id); }

		uint16_t getPacketID() const 
		{
			uint8_t offset = fixed_header::getVarHeaderOffset();
			return ((at(offset) << 8) | at(offset + 1));	
		}

		void set(uint16_t packet_id = 0)
		{
			fixed_header::setType(CTRL_TYPE::PUBACK);
			fixed_header::setFlag(FLAG_PUBACK);
			puback_var_header var_header(packet_id);
			fixed_header::encode_remaining_length(var_header.size());
			fixed_header::append_field(var_header);
		};		
	};
}