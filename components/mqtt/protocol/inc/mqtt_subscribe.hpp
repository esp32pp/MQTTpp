#pragma once

#include "mqtt_base.hpp"
#include "id_var_header.hpp"
#include <utility>

namespace mqtt
{
	// *********************************************************************************
	// 	SUBSCRIBE
	// *********************************************************************************

	typedef id_var_header subscribe_var_header;

	// ----------------------------------------------------------------------------------------

	typedef std::vector<std::pair<std::string, uint8_t> > topic_list_t;

	class subscribe_packet : public fixed_header
	{
	public:
		subscribe_packet(const std::string& topic, uint16_t packet_id = 0) { set(topic, packet_id); }
		subscribe_packet(const topic_list_t& topic_list, uint16_t packet_id = 0) { set(topic_list, packet_id); }

		inline void set(const std::string& topic, uint8_t qos = 0, uint16_t packet_id = 0)
		{ set(topic_list_t(1, std::make_pair(topic, qos)), packet_id); }

		void set(const topic_list_t& topic_list, uint16_t packet_id = 0)
		{
			fixed_header::setType(CTRL_TYPE::SUBSCRIBE);
			fixed_header::setFlag(FLAG_SUBSCRIBE);
			subscribe_var_header var_header(packet_id);

			for(auto topic : topic_list)
			{	
				var_header.append_field(mqtt::text_field(topic.first));		// topic name
				var_header.append_field(mqtt::field(1, topic.second & 0x03));	// qos
			}

			fixed_header::encode_remaining_length(var_header.size());
			fixed_header::append_field(var_header);
		};		
	};
}