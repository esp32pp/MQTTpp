#pragma once

#include "mqtt_base.hpp"

namespace mqtt
{
	// *********************************************************************************
	// 	CONNACK
	// *********************************************************************************
	
	enum class CONNECT_RETURN : uint8_t
	{
		ACCEPTED 			= 0,
		BAD_PROTOCOL	 	= 1,
		ID_REJECTED 		= 2,
		SERVER_UNAVAILABLE 	= 3,
		BAD_CREDENTIALS 	= 4,
		NOT_AUTHORIZED 		= 5
	};

	// ----------------------------------------------------------------------------------------

	class connack_var_header : public field
	{
	public:
		connack_var_header() : field(2) {};
		connack_var_header(uint8_t ack_flags, uint8_t code) : field(2) 
		{
			if(code > 5)
				throw std::range_error("Connect return code undefined");
				// throw(MQTT_ERR_BAD_CONNECT_CODE);

			session_present = ack_flags & 0x01;
			return_code = static_cast<CONNECT_RETURN>(code);
		};

		bool 			session_present = false;
		CONNECT_RETURN 	return_code 	= CONNECT_RETURN::ACCEPTED;
	};

	// ----------------------------------------------------------------------------------------

	class connack_packet : public fixed_header
	{
	public:
		connack_packet(){ set(); }	
		connack_packet(uint8_t ack_flags, uint8_t code) { set(ack_flags, code); }
		connack_packet(const mqtt::fixed_header& obj) 
		{
			fixed_header::insert(fixed_header::begin(), obj.begin(), obj.end());
			fixed_header::resize(obj.size());
		}

		void set(uint8_t ack_flags = 0, uint8_t code = 0)
		{
			fixed_header::setType(CTRL_TYPE::CONNACK);
			fixed_header::setFlag(FLAG_CONNACK);
			connack_var_header var_header(ack_flags, code);
			fixed_header::encode_remaining_length(var_header.size());
			fixed_header::append_field(var_header);
		};

		bool isAccepted() const { return (at(3) == 0); }
		CONNECT_RETURN getReturnCode() const { return static_cast<CONNECT_RETURN>(at(3)); }		
	};
}