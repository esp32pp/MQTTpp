#pragma once

#include "mqtt_base.hpp"
#include "id_var_header.hpp"

namespace mqtt
{
	// *********************************************************************************
	// 	SUBACK
	// *********************************************************************************

	typedef id_var_header suback_var_header;

	// ----------------------------------------------------------------------------------------

	enum class SUBACK_RETURN : uint8_t
	{
		QOS0 	= 0x00,
		QOS1 	= 0x01,
		QOS2 	= 0x02,
		FAILED 	= 0x80
	};

	class suback_packet : public fixed_header
	{
	public:
		// suback_packet(){ set(); }	
		suback_packet(SUBACK_RETURN return_code, uint16_t packet_id) { set(return_code, packet_id); }
		suback_packet(const std::vector<SUBACK_RETURN>& return_codes, uint16_t packet_id = 0) { set(return_codes, packet_id); }

		uint16_t getPacketID() const 
		{
			uint8_t offset = fixed_header::getVarHeaderOffset();
			return ((at(offset) << 8) | at(offset + 1));	
		}

		inline void set(SUBACK_RETURN return_code, uint16_t packet_id = 0)
		{
			set(std::vector<SUBACK_RETURN>(1, return_code), packet_id);
		}

		void set(const std::vector<SUBACK_RETURN>& return_codes, uint16_t packet_id = 0)
		{
			fixed_header::setType(CTRL_TYPE::SUBACK);
			fixed_header::setFlag(FLAG_SUBACK);
			suback_var_header var_header(packet_id);

			for(auto elem : return_codes)
				var_header.append_field(mqtt::field(static_cast<uint8_t>(elem)));

			fixed_header::encode_remaining_length(var_header.size());
			fixed_header::append_field(var_header);
		};		
	};
}