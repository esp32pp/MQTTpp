#pragma once

/* This header defines all packets, defined by the MQTT 3.1.1 standard.
 * Each packet with its corresponding types and sub-packets is defined in the
 * respective header file, included below. 
 * 
 * The overall structure is as hierarchical as possible. Therefore packet header may include other
 * sub-components for construction. */

#include "mqtt_connect.hpp"
#include "mqtt_connack.hpp"

#include "mqtt_ping.hpp"
#include "mqtt_disconnect.hpp"	

#include "mqtt_publish.hpp"
#include "mqtt_puback.hpp"
#include "mqtt_pubrec.hpp"
#include "mqtt_pubrel.hpp"
#include "mqtt_pubcomp.hpp"

#include "mqtt_subscribe.hpp"
#include "mqtt_suback.hpp"

#include "mqtt_unsubscribe.hpp"
#include "mqtt_unsuback.hpp"


namespace mqtt
{}