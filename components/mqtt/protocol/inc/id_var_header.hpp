#pragma once

#include "mqtt_base.hpp"

namespace mqtt
{
	class id_var_header : public mqtt::field
	{
	public:
		id_var_header() : id_var_header(0) {};
		id_var_header(uint16_t id) : field(2) 
		{
			at(0) = (id >> 8) & 0xff;
			at(1) = id & 0xff;
		};
	};
}