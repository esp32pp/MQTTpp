#pragma once

#include <string>
#include <iostream>
#include <exception>
	

namespace mqtt
{
	// *********************************************************************************
	// 	ERROR-HANDLING
	// *********************************************************************************

	/**
	 * @brief An exception object that handles protocol related faults
	 * @details If a protocol violation is detected, this error class is thrown with a copy of relevant
	 * debug information.
	 * 
	 * @param whatStr Descriptive string of the error that caused the exception
	 */
	class protocol_exception : public std::logic_error
	{
	public:
		protocol_exception(const std::string& whatStr) : std::logic_error(whatStr) {};		
		// protocol_exception(const std::string& whatStr, const mqtt::field& packet_copy) : std::logic_error(whatStr), packet(packet_copy) {};	

		// mqtt::field packet;	
	};
}