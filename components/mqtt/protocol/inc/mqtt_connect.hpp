#pragma once

#include "mqtt_base.hpp"
	

namespace mqtt
{

	// *********************************************************************************
	// 	CONNECT
	// *********************************************************************************

	class protocol_name : public field
	{
	public:
		protocol_name() : field(6) 
		{
			at(0) = 0x00;	// LEN MSB
			at(1) = 0x04;	// LEN LSB
			at(2) = 0x4D;	// 'M'
			at(3) = 0x51;	// 'Q'
			at(4) = 0x54;	// 'T'
			at(5) = 0x54;	// 'T'
		};		
	};

	// ----------------------------------------------------------------------------------------

	class protocol_level : public field
	{
	public:
		protocol_level() : field(1) 
		{
			at(0) = 0x04;	
		};		
	};

	// ----------------------------------------------------------------------------------------

	class connect_flag : public field
	{
	public:
		connect_flag() : field(1) {};

		connect_flag(const con_settings_t& cfg) : field(1) 
		{
			if(cfg.will_qos > 3)
				throw std::range_error("QoS unsupported (must be < 3)");
				// throw(MQTT_ERR_INVALID_QOS);

			at(0) = ((cfg.user.size() > 0 ? 1 : 0) << 7);	
			at(0) |= ((cfg.pwd.size() > 0 ? 1 : 0) << 6);	
			at(0) |= ((cfg.will_retain) << 5);	
			at(0) |= ((cfg.will_qos & 0x03) << 3);	
			at(0) |= (cfg.will << 2);	
			at(0) |= ((cfg.clean_session) << 1);	
		};		
	};

	// ----------------------------------------------------------------------------------------

	class keep_alive : public field
	{
	public:
		keep_alive() : field(2) { at(0) = 0x00; at(1) = 0x00; };
		keep_alive(uint16_t keep_alive) : field(2) 
		{
			at(0) = (keep_alive >> 8) & 0xff;	
			at(1) = keep_alive & 0xff;	
		};		
	};

	// ----------------------------------------------------------------------------------------

	class connect_var_header : public protocol_name
	{
	public:
		connect_var_header(){};
		connect_var_header(const con_settings_t& cfg)
		{
			append_field(protocol_level());
			append_field(connect_flag(cfg));
			append_field(keep_alive(cfg.keep_alive_time));
		}

	};

	// ----------------------------------------------------------------------------------------

	class connect_payload : public text_field
	{
	public:
		connect_payload(){};
		connect_payload(const con_settings_t& cfg) : text_field(cfg.clientID)
		{
			// printf("Payload:\n");

			if(cfg.will)
			{
				// printf("Appending Will (%d, %d)\n", cfg.will_topic.length(), cfg.will_str.length());
				text_field::append_field(text_field(cfg.will_topic));
				text_field::append_field(text_field(cfg.will_str));
			}

			if(cfg.user.size() > 0)
			{
				// printf("Appending Username of length %d\n", cfg.user.length());
				text_field::append_field(text_field(cfg.user));
			}
			
			if(cfg.pwd.size() > 0)
			{
				// printf("Appending Password of length %d\n", cfg.pwd.length());
				text_field::append_field(text_field(cfg.pwd));
			}
		};
	};

	// ----------------------------------------------------------------------------------------

	class connect_packet : public fixed_header
	{
	public:
		connect_packet(){ set(con_settings_t()); }	
		connect_packet(const con_settings_t& cfg) { set(cfg); }

		void set(const con_settings_t& cfg)
		{
			fixed_header::setType(CTRL_TYPE::CONNECT);
			fixed_header::setFlag(FLAG_CONNECT);
			connect_var_header var_header(cfg);
			// printf("Appending payload to variable header\n");
			var_header.append_field(connect_payload(cfg));
			fixed_header::encode_remaining_length(var_header.size());
			// printf("Adding payload and variable header to fixed header\n");
			fixed_header::append_field(var_header);
		};		
	};
}