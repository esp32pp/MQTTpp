#pragma once

#include "mqtt_base.hpp"

namespace mqtt
{
	// *********************************************************************************
	// 	DISCONNECT
	// *********************************************************************************
	
	class disconnect_packet : public fixed_header
	{
	public:
		disconnect_packet()
		{ 
			fixed_header::setType(CTRL_TYPE::DISCONNECT);
			fixed_header::setFlag(FLAG_DISCONNECT); 
			fixed_header::encode_remaining_length(0);	// remaining length
		}
	};
}