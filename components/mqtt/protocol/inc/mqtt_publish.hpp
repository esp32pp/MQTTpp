#pragma once

#include "mqtt_base.hpp"

namespace mqtt
{
	// *********************************************************************************
	// 	PUBLISH
	// *********************************************************************************

	class message : public field
	{
	public:
		message() {};
		message(const uint32_t len) : mqtt::field(len) {};
		message(const std::string& top, const std::string& msg, uint8_t QoS, bool retain_msg) :
		mqtt::field(msg), topic(top), qos(QoS), retain(retain_msg)
		{};

		std::string to_string() const
		{
			return std::string(field::begin(), field::end());
		}
	
		std::string topic 		= "";
		uint8_t 	qos 		= 0;
		bool 		retain 		= false;
		uint16_t 	packetID 	= 0;
		bool 		resend 		= false;
	};

	// ----------------------------------------------------------------------------------------

	class publish_var_header : public text_field
	{
	public:
		publish_var_header(){};
		publish_var_header(const mqtt::message& msg) : mqtt::text_field(msg.topic)
		{
			if(msg.qos > 0)
			{
				field::push_back((msg.packetID >> 8) & 0xff);
				field::push_back(msg.packetID & 0xff);
			}
		}
	};

	// ----------------------------------------------------------------------------------------
	
	class publish_packet : public fixed_header
	{
	public:
		publish_packet(){ encode(mqtt::message()); }	
		publish_packet(const mqtt::message& msg) { encode(msg); }

		uint16_t getPacketID() const 
		{
			if(fixed_header::getQoS() > 0)
			{
				uint32_t offset = fixed_header::getVarHeaderOffset();
				offset += (at(offset) << 8) | at(offset + 1);
				offset += 2;
				return ((at(offset) << 8) | at(offset + 1));
			}
			else
				return 0;
		}

		std::string getTopic() const 
		{
			uint32_t offset = fixed_header::getVarHeaderOffset();
			uint16_t topic_len = (at(offset) << 8) | at(offset + 1);
			offset += 2;
			return std::string(fixed_header::begin() + offset, fixed_header::begin() + offset + topic_len);
		}

		std::vector<uint8_t> getPayload() const
		{
			uint32_t offset = fixed_header::getVarHeaderOffset();
			offset += (at(offset) << 8) | at(offset + 1);
			offset += 2;
			return std::vector<uint8_t>(fixed_header::begin() + offset, fixed_header::end());
		}

		message decode() const 
		{
			uint32_t offset 	= fixed_header::getVarHeaderOffset();
			offset += (at(offset) << 8) | at(offset + 1);
			uint32_t packet_len = reinterpret_cast<fixed_header*>(const_cast<publish_packet*>(this))->decode_remaining_length();
			
			message msg(packet_len);
			for(auto it = offset; it < packet_len; ++it)
				msg[it - offset] = at(it + 2);
			// message msg 	= static_cast<mqtt::message>(getPayload());
			// message msg(fixed_header::begin() + offset, fixed_header::end());

			msg.qos 		= fixed_header::getQoS();
			msg.retain 		= fixed_header::isRetained();
			msg.packetID 	= (at(offset) << 8) | at(offset + 1);
			msg.resend 		= fixed_header::isResent();
			msg.topic 		= getTopic();

			return msg;
		}

		void encode(const mqtt::message& msg)
		{			
			fixed_header::setType(CTRL_TYPE::PUBLISH);
			fixed_header::setFlag(FLAG_PUBLISH(msg.resend, msg.qos, msg.retain)); 
			publish_var_header var_header(msg);
			var_header.append_field(msg);
			fixed_header::encode_remaining_length(var_header.size());
			fixed_header::append_field(var_header);
		};		
	};
}