#pragma once

#include "mqtt_base.hpp"
#include "id_var_header.hpp"
#include <utility>

namespace mqtt
{
	// *********************************************************************************
	// 	UNSUBSCRIBE
	// *********************************************************************************

	typedef id_var_header unsubscribe_var_header;

	// ----------------------------------------------------------------------------------------

	typedef std::vector<std::pair<std::string, uint8_t> > topic_list_t;

	class unsubscribe_packet : public fixed_header
	{
	public: // ENCODE
		unsubscribe_packet(const std::string& topic, uint16_t packet_id = 0) { set(topic, packet_id); }
		unsubscribe_packet(const topic_list_t& topic_list, uint16_t packet_id = 0) { set(topic_list, packet_id); }

		inline void set(const std::string& topic, uint8_t qos = 0, uint16_t packet_id = 0)
		{
			set(topic_list_t(1, std::make_pair(topic, qos)), packet_id);
		}

		void set(const topic_list_t& topic_list, uint16_t packet_id = 0)
		{
			fixed_header::setType(CTRL_TYPE::UNSUBSCRIBE);
			fixed_header::setFlag(FLAG_UNSUBSCRIBE);
			unsubscribe_var_header var_header(packet_id);

			for(auto topic : topic_list)
			{	
				var_header.append_field(mqtt::text_field(topic.first));
				var_header.append_field(mqtt::field(topic.second & 0x03));
			}

			fixed_header::encode_remaining_length(var_header.size());
			fixed_header::append_field(var_header);
		};
				
	public: // DECODE
		uint16_t getPacketID() const 
		{
			uint8_t offset = fixed_header::getVarHeaderOffset();
			return ((at(offset) << 8) | at(offset + 1));	
		}

	};
}