#pragma once

#include "mqtt_base.hpp"	

namespace mqtt
{
	// *********************************************************************************
	// 	PINGREQ
	// *********************************************************************************
	
	class pingreq_packet : public mqtt::fixed_header
	{
	public:
		pingreq_packet()
		{ 
			fixed_header::setType(CTRL_TYPE::PINGREQ);
			fixed_header::setFlag(FLAG_PINGREQ);  
			fixed_header::encode_remaining_length(0);	// remaining length
		}
	};

	// *********************************************************************************
	// 	PINGRESP
	// *********************************************************************************

	class pingresp_packet : public mqtt::fixed_header
	{
	public:
		pingresp_packet()
		{ 
			fixed_header::setType(CTRL_TYPE::PINGRESP);
			fixed_header::setFlag(FLAG_PINGRESP);  
			fixed_header::encode_remaining_length(0);	// remaining length
		}
	};
}