#pragma once

#include <vector>
#include <string>
#include <algorithm>
#include <bitset>
#include <iostream>

#include "mqtt_error.hpp"	

namespace mqtt
{
	// *********************************************************************************
	// 	BASE
	// *********************************************************************************

	/**
	 * @brief Control Package Type
	 */
	enum class CTRL_TYPE : uint8_t // From the MQTT 3.1.1 standard documentation
	{
		CONNECT 	= 1,	/*!< Client request to connect to Server */
		CONNACK 	= 2,	/*!< Connect acknowledgment */
		PUBLISH 	= 3,	/*!< Publish message */
		PUBACK 		= 4,	/*!< Publish acknowledgment */
		PUBREC 		= 5,	/*!< Publish received (assured delivery part 1) */
		PUBREL 		= 6,	/*!< Publish release (assured delivery part 2) */
		PUBCOMP 	= 7,	/*!< Publish complete (assured delivery part 3) */
		SUBSCRIBE 	= 8,	/*!< Client subscribe request */
		SUBACK 		= 9,	/*!< Subscribe acknowledgment */
		UNSUBSCRIBE = 10,	/*!< Unsubscribe request */
		UNSUBACK 	= 11,	/*!< Unsubscribe acknowledgment */
		PINGREQ 	= 12,	/*!< PING request */
		PINGRESP 	= 13,	/*!< PING response */
		DISCONNECT 	= 14	/*!< Client is disconnecting */
	};

	std::string to_string(const CTRL_TYPE& rhs)
	{
		switch(rhs)
			{
				case mqtt::CTRL_TYPE::CONNECT: 		return "CONNECT";
				case mqtt::CTRL_TYPE::CONNACK: 		return "CONNACK";
				case mqtt::CTRL_TYPE::PUBLISH:		return "PUBLISH";
				case mqtt::CTRL_TYPE::PUBACK:		return "PUBACK";
				case mqtt::CTRL_TYPE::PUBREC:		return "PUBREC";
				case mqtt::CTRL_TYPE::PUBREL:		return "PUBREL";
				case mqtt::CTRL_TYPE::PUBCOMP:		return "PUBCOMP";
				case mqtt::CTRL_TYPE::SUBSCRIBE:	return "SUBSCRIBE";
				case mqtt::CTRL_TYPE::SUBACK:		return "SUBACK";
				case mqtt::CTRL_TYPE::UNSUBSCRIBE:	return "UNSUBSCRIBE";
				case mqtt::CTRL_TYPE::UNSUBACK:		return "UNSUBACK";	
				case mqtt::CTRL_TYPE::PINGREQ:		return "PINGREQ";
				case mqtt::CTRL_TYPE::PINGRESP:		return "PINGRESP";
				case mqtt::CTRL_TYPE::DISCONNECT:	return "DISCONNECT";
				default:
					throw protocol_exception("Unknown control package type");					
			}
	}

	std::ostream& operator<<(std::ostream& stream, const CTRL_TYPE& rhs)
	{ return stream << to_string(rhs); }

	#define FLAG_CONNECT 			0x00
	#define FLAG_CONNACK 			0x00
	#define FLAG_PUBLISH(d, q, r) 	((((d) & 0x01) << 3) | (((q) & 0x03) << 1) | ((r) & 0x01)) 
	#define FLAG_PUBACK 			0x00
	#define FLAG_PUBREC 			0x00
	#define FLAG_PUBREL 			0x02
	#define FLAG_PUBCOMP 			0x00
	#define FLAG_SUBSCRIBE 			0x02
	#define FLAG_SUBACK 			0x00
	#define FLAG_UNSUBSCRIBE 		0x02
	#define FLAG_UNSUBACK 			0x00
	#define FLAG_PINGREQ 			0x00
	#define FLAG_PINGRESP 			0x00
	#define FLAG_DISCONNECT			0x00

	typedef struct connect_settings
	{
		std::string clientID;
		std::string user;
		std::string pwd;
		bool will;
		bool will_retain; 
		uint8_t will_qos;
		std::string will_topic; 
		std::string will_str; 
		bool clean_session;
		uint16_t keep_alive_time;

		connect_settings() 
		: clientID(), user(), pwd(), will(false), will_retain(false), will_qos(0), will_topic(), will_str(),
		clean_session(true), keep_alive_time(0)
		{};

		connect_settings(	const std::string& client_str,
							const std::string& userStr,
							const std::string& pwdStr,
							bool will_flg,
							bool will_retain_flg, 
							uint8_t will_qos_lvl,
							const std::string& will_topic_str, 
							const std::string& will, 
							bool clean_session_flg,
							uint16_t keep_alive) 
		: clientID(client_str), user(userStr), pwd(pwdStr), 
		will(will_flg), will_retain(will_retain_flg), will_qos(will_qos_lvl), will_topic(will_topic_str), will_str(will), 
		clean_session(clean_session_flg), keep_alive_time(keep_alive)
		{};

		void setWill(const std::string& topic, const std::string& msg, uint8_t qos = 0, bool retain = false)
		{
			will 		= true;
			will_retain = retain;
			will_qos 	= qos;
			will_topic 	= topic;
			will_str 	= msg;
		}

		void setCredentials(const std::string& strUser, const std::string& strPwd)
		{
			user = strUser;
			pwd = strPwd;
		}

	} con_settings_t;

	// ----------------------------------------------------------------------------------------

	/**
	 * @brief Basic type that underlies every packet class
	 * @details This class is the most basic base from which all other packet-classes are derived. It specializes
	 * a std::vector with an uint8_t type. It adds a utility function for appending another field as well as a 
	 * function to print the content nicely into a console output.
	 */
	class field : public std::vector<uint8_t>
	{
	public:
		field() {};
		field(uint32_t len) : std::vector<uint8_t>(len) {};
		field(uint32_t len, uint8_t init_val) : std::vector<uint8_t>(len, init_val) {};

		/**
		 * @brief Copy constructor for the mqtt::field
		 * @details Copies the passed object into the underlying vector
		 * 
		 * @param obj Object from which the instance should be constructed
		 */
		field(const std::vector<uint8_t>& obj) : std::vector<uint8_t>(obj.begin(), obj.end()) {}

		field(const mqtt::field& obj) : std::vector<uint8_t>(obj.begin(), obj.end()) {};

		field(const std::string& obj) 
		{ 
			field::reserve(obj.size());
			field::insert(field::begin(), obj.begin(), obj.end()); 
		}

		/**
		 * @brief Appends another mqtt::field at the end of the instance
		 * @details This method concatenates the second mqtt::field with this instance
		 * 
		 * @param new_field Field to be appended
		 */
		inline void append_field(const field& new_field) 
		{ 
			try
			{
				field::reserve(field::size() + new_field.size());
				field::insert(field::end(), new_field.begin(), new_field.end());
			}
			catch(...)
			{
				printf("Exception occurred while appending field of size %d\n", new_field.size());
				throw;
			}
		};

		/**
		 * @brief Prints the complete field in a formatted manner
		 * @details Prints decimal, hexadecimal, character and binary representation of each byte
		 */
		void print()
		{
			std::bitset<8> bit;
			for(auto it =0; it < size(); ++it)
			{
				bit = at(it);
				if(at(it) == '\n')
					printf("%03d\t%#4x\t\\n\t", at(it), at(it));
				else if(at(it) == 0x08)
					printf("%03d\t%#4x\t \t", at(it), at(it));
				else if(at(it) == '\t')
					printf("%03d\t%#4x\t \t", at(it), at(it));
				else
					printf("%03d\t%#4x\t%c\t", at(it), at(it), at(it));
				std::cout << bit << std::endl;
			}
		};
	};

	// ----------------------------------------------------------------------------------------

	/**
	 * @brief Extends the mqtt::field class for text. 
	 * @details The MQTT standard defines a 2-byte length field in front of a string field. This class sets
	 * the length field accordingly and adds the string into the mqtt::field.
	 */
	class text_field : public field
	{
	public:
		text_field() : field(2) {};

		text_field(const std::string& str) : field(2)	// using the constructor for string is not possible, because we need two fields in front for the length
		{
			if(str.length() > 0xffff)
				throw std::length_error("String too long");

			if((str == "") || (str.empty()))
			{
				at(0) = 0;
				at(1) = 0;
			}
			else
			{
				field::reserve(2 + str.size());
				field::insert(field::begin() + 2, str.begin(), str.end());
				setLengthField(str);
			}
		};	

		void set(const std::string& str)
		{
			if(str.length() > 0xffff)
				throw std::length_error("String too long");

			field::insert(field::begin() + 2, str.begin(), str.end());
			field::resize(str.length() + 2);
			setLengthField(str);
		};

		std::string get()
		{
			std::string result;
			result.reserve((at(0) << 8) | (at(1)));
			std::copy(field::begin() + 2, field::end(), result.begin());
			return result;
		}
	private:
		inline void setLengthField(const std::string& str)
		{
			field::at(0) = (str.length() >> 8) & 0xff;
			field::at(1) = str.length() & 0xff;
		}		
	};

	// ----------------------------------------------------------------------------------------

	class fixed_header : public mqtt::field
	{
	public:
		fixed_header() : field(2) 
		{ 
			setFlag(0x00); 
			setType(CTRL_TYPE::CONNECT);
		};

		fixed_header(const mqtt::field& obj) : mqtt::field(obj)
		{
			checkIntegrity();
			decode_remaining_length();
		}

		inline void setFlag(uint8_t flag)
		{
			field::at(0) = ( (field::at(0) & 0xf0) | (flag & 0x0f) ); 
		}

		inline void setType(CTRL_TYPE type)
		{
			field::at(0) = (((static_cast<uint8_t>(type) << 4) & 0xf0 ) | (field::at(0) & 0x0f) ); 
		}

		inline CTRL_TYPE getType() const { return static_cast<mqtt::CTRL_TYPE>((field::at(0) >> 4) & 0x0f); }

		/**
		 * @brief Encodes the remaining length and writes it to the data field
		 * @details The remaining-length field is encoded in a 1-4 byte long field. This method encodes a
		 * decimal byte-count value into the format specified by the standard.
		 * 
		 * @note The alorithm is the one proposed in the MQTT 3.1.1 standard 
		 * 
		 * @param len Remaining length in bytes
		 */
		void encode_remaining_length(uint32_t len)
		{
			/* Get the number of bytes needed for the remaining length field
			 * and reserve the memory.
			 * !!! this might be premature optimization !!!*/
			uint_fast8_t it = 0;
			uint_fast32_t len_copy(len);
			do
			{
				it++;
				len_copy = len_copy / 128;
			}while(len_copy > 0);
			mqtt::field::reserve(it + 1);
			it = 1;
			do
			{
				if(it < mqtt::field::size())
					at(it) = (len % 128);
				else
					push_back(len % 128);

				len /= 128;
				// if there are more data to encode, set the top bit of this byte
				if(len > 0)
					at(it) |= 128;

				it++;
			}while(len > 0);
		}

		/**
		 * @brief Decodes the remaining-length field(s) and returns the decimal byte value
		 * @details The remaining length is decoded in a 1-4 byte long field. This method decodes the value
		 * and returns the decimal number of bytes.
		 * 
		 * @note The alorithm is the one proposed in the MQTT 3.1.1 standard 
		 * 
		 * @return Remaining length in bytes
		 */
		uint32_t decode_remaining_length()
		{
			uint32_t 	val 	= 0;
			uint32_t 	mult 	= 1;
			uint8_t 	it 		= 0;

			do
			{
				it++;
				val += (at(it) & 127) * mult;
				mult *= 128;
				if(mult > (128*128*128))
					throw std::length_error("Decoded message length exceeds 4 bytes");

			}while((at(it) & 128) != 0); // "it" is already incremented, therefore at(it) needs to be called instead of at(it+1) 
						
			return val;
		}

		/**
		 * @brief Returns the remaining length field (decoded)
		 * @return Remaining length in bytes
		 */
		inline uint32_t getRemainingLength()
		{ 
			return decode_remaining_length();
		}

		/**
		 * @brief Checks the integrity of the header and throws an exception if there is a mismatch
		 * @details The MQTT standard requires the control type and header flags to match (according to the standard)
		 * and for any side to close the connection if it does not. This function reads the control type and checks the flag for
		 * the right value. If a mismatch is detected, an exception is thrown.
		 */
		void checkIntegrity()
		{
			switch(static_cast<mqtt::CTRL_TYPE>((field::at(0) >> 4) & 0x0f))
			{
				case mqtt::CTRL_TYPE::CONNECT: 
					compare_flag(FLAG_CONNECT);
					break;
				case mqtt::CTRL_TYPE::CONNACK: 
					compare_flag(FLAG_CONNACK);
					break;
				case mqtt::CTRL_TYPE::PUBLISH:
					/*Comparing the flag for a publish packet requires knowledge of the expected flags
					 * meaning, DUP, QOS, and RETAIN which is normally not given*/
					// compare_flag(FLAG_PUBLISH);
					break;
				case mqtt::CTRL_TYPE::PUBACK:
					compare_flag(FLAG_PUBACK);
					break;
				case mqtt::CTRL_TYPE::PUBREC:
					compare_flag(FLAG_PUBREC);
					break;
				case mqtt::CTRL_TYPE::PUBREL:
					compare_flag(FLAG_PUBREL);
					break;
				case mqtt::CTRL_TYPE::PUBCOMP:
					compare_flag(FLAG_PUBCOMP);
					break;
				case mqtt::CTRL_TYPE::SUBSCRIBE:
					compare_flag(FLAG_SUBSCRIBE);
					break;
				case mqtt::CTRL_TYPE::SUBACK:
					compare_flag(FLAG_SUBACK);
					break;
				case mqtt::CTRL_TYPE::UNSUBSCRIBE:
					compare_flag(FLAG_UNSUBSCRIBE);
					break;
				case mqtt::CTRL_TYPE::UNSUBACK:
					compare_flag(FLAG_UNSUBACK);
					break;
				case mqtt::CTRL_TYPE::PINGREQ:
					compare_flag(FLAG_PINGREQ);
					break;
				case mqtt::CTRL_TYPE::PINGRESP:
					compare_flag(FLAG_PINGRESP);
					break;
				case mqtt::CTRL_TYPE::DISCONNECT:
					compare_flag(FLAG_DISCONNECT);
					break;
				default:
					throw protocol_exception("Unknown control package type");					
			}
		}
			
		inline uint8_t 	getQoS() const { return (at(0) >> 1) & 0x03; }
		inline bool 	isRetained() const { return (at(0) & 0x01); }
		inline bool 	isResent() const { return ((at(0) >> 3) & 0x01); }
		inline uint8_t 	getVarHeaderOffset() const 
		{
			uint8_t var_header_offset = 0;
			do { var_header_offset++; } while((at(var_header_offset) & 128) != 0);
			return var_header_offset + 1;
		}

	private:

		inline void compare_flag(uint8_t flag)
		{
			if((field::at(0) & 0x0f) != flag)
				throw protocol_exception("Control type does not match flag");
		}
	};
}