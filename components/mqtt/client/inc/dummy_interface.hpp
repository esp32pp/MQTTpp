#pragma once

#include <string>

namespace mqtt
{
	class dummy_network_interface
	{
	public:
		dummy_network_interface() 
		{
			_isOpen = false;
		};

		inline void open(const std::string& hostname, uint16_t port) { _isOpen = true; }
		inline void close() { _isOpen = false; }
		inline bool isOpen() const { return _isOpen; };

		template<typename T>
		void operator<<(const T& packet) {}

		template<typename T>
		void operator>>(const T& rhs) {};

	private: 
		bool _isOpen;
	};
}