#pragma once

#include <string>
#include <stdio.h>
#include <vector>
#include <deque>
#include <memory>

#include "mqtt_3_1_1.hpp"

namespace mqtt
{
	/**
	 * @brief Exception class for information transport
	 * @details Keeps information about variables when an exception is
	 * thrown.
	 * 
	 * @param whatStr Descriptive short string about the cause
	 */
	class mqtt_protocol_error : public std::logic_error
	{
	public:
		/**
		 * @brief Constructor with cause-string
		 * @details Constructs the error class with a descriptive string
		 * 
		 * @param whatStr Cause for the exception
		 */
		mqtt_protocol_error(const std::string& whatStr) : std::logic_error(whatStr) {};		
		mqtt_protocol_error(const std::string& whatStr, mqtt::CONNECT_RETURN ret_code) : std::logic_error(whatStr), return_code(ret_code) {};		
	
		/* Add variables here that keep state information at exception time
		 * to help resolving the cause and either give detailed information
		 * about the cause or enough information to recover.*/
		mqtt::CONNECT_RETURN return_code;
	};

	// --------------------------------------------------------------------------------------------------------------------
	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	class mqtt_client_protocol
	{
	public:
		mqtt_client_protocol();
		mqtt_client_protocol(const std::string& client_id);
		~mqtt_client_protocol() { disconnect_send(); };
		
		
	public:	// Protocol actions
		void connect_send();
		void disconnect_send();

		void subscribe_send(const topic_list_t& topics);
		void unsubscribe_send(const topic_list_t& topics);

		void ping_send();

		void publish_send(const mqtt::message& msg);

	public: // Configuration
		void setCredentials(const std::string& strUser, const std::string& strPwd);
		void setKeepAlive(uint16_t keep_alive_sec);
		void setWill(const std::string& topic, const std::string& msg, uint8_t qos = 0, bool retain = false);
		void setHost(const std::string& hostname, uint16_t port);

	public: // Utility
		inline bool isConnected() const { return _isConnected; };

	private:
		void clear_queues();
		void clear_message_id_list();
		void clear_stored_messages();
		uint16_t getUnusedID();

		void setID(uint16_t id, bool val);
		inline void setIDUsed(uint16_t id) { setID(id, true); };
		inline void discardMessage(uint16_t id) { setID(id, false); };
		void discardMessage(const mqtt::publish_packet& packet);

		void acknoledgePUBREC(uint16_t id);

	protected:
		void receive();

		virtual void onPublishReceived(const mqtt::publish_packet& obj) = 0;

	protected:
		NETWORK 		_interface;
		std::string 	_hostname;
		uint16_t 		_port;
		con_settings_t 	_mqtt_settings;
		bool 			_isConnected;
		bool 			_pingresp_awaited;
		mqtt::publish_packet _outgoing_packet;
		std::vector<std::pair<uint16_t, bool> > 			_inflight_messag_ids;
		std::deque<std::unique_ptr<mqtt::publish_packet> > 	_stored_messages;
		SemaphoreHandle_t _id_list_mutex = nullptr;
	};

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::mqtt_client_protocol() : mqtt_client_protocol("")
	{}

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::mqtt_client_protocol(const std::string& client_id)
	{
		_mqtt_settings.clientID = client_id;
		_isConnected = false;
		_pingresp_awaited = false;
		clear_message_id_list();
		_id_list_mutex 	= xSemaphoreCreateMutex();
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::connect_send()
	{
		// If the client is still connected to a broker, disconnect it first
		if(isConnected())
			disconnect_send();

		// If the network interface is not open yet, open it
		if(!_interface.isOpen())
			_interface.open(_hostname, _port);
		
		// Send the CONNECT packet and wait for the CONNACK response
		_interface << mqtt::connect_packet(_mqtt_settings);
		mqtt::connack_packet ack_pkg;
		_interface >> ack_pkg;
		if(!ack_pkg.isAccepted())
			throw mqtt_protocol_error("Broker rejected connection", ack_pkg.getReturnCode());

		// Set internal status variables accordingly
		/* From the MQTT 3.1.1 standard:
		 * When a Client reconnects with CleanSession set to 0, both the Client and Server MUST re-send any
		 * unacknowledged PUBLISH Packets (where QoS > 0) and PUBREL Packets using their original Packet
		 * Identifiers [MQTT-4.4.0-1]. This is the only circumstance where a Client or Server is REQUIRED to
		 * redeliver messages.*/
		if(_mqtt_settings.clean_session)
			clear_queues();
		
		_isConnected = true;	// is already false, because disconnect() was called
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::disconnect_send()
	{
		_isConnected = false;
		if(_interface.isOpen())
		{
			_interface << mqtt::disconnect_packet();
			vTaskDelay(500/portTICK_PERIOD_MS);
			_interface.close();
		}
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::subscribe_send(const topic_list_t& topics)
	{
		if(isConnected())
		{
			try
			{
				_interface << mqtt::subscribe_packet(topics, getUnusedID());
			}
			catch(mqtt_protocol_error& e)
			{
				// The client has run out of free message IDs.
				// For the moment: just pass the error through without disconnecting
				throw; 
			}
			catch(...)	// something went wrong with the packet transmission, which means a socket error
			{
				disconnect_send();
				throw;
			}
		}
		else
			throw mqtt_protocol_error("Not connected to broker");
	}

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::unsubscribe_send(const topic_list_t& topics)
	{
		if(isConnected())
		{
			try
			{
				uint16_t packet_id = getUnusedID();
				_interface << mqtt::unsubscribe_packet(topics, packet_id);
			}
			catch(mqtt_protocol_error& e)
			{
				// The client has run out of free message IDs.
				// For the moment: just pass the error through without disconnecting
				throw; 
			}
			catch(...)	// something went wrong with the packet transmission, which means a socket error
			{
				disconnect_send();
				throw;
			}
		}
		else
			throw mqtt_protocol_error("Not connected to broker");
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::ping_send()
	{
		if(isConnected())
		{
			try
			{
				_interface << mqtt::pingreq_packet();
				_pingresp_awaited = true;
			}
			catch(...)
			{
				disconnect_send();
				throw;
			}
		}
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::publish_send(const mqtt::message& msg)
	{
		if(isConnected())
		{
			try
			{
				if(msg.qos > 0)
				{
					const_cast<mqtt::message&>(msg).packetID = getUnusedID();
				
					std::unique_ptr<mqtt::publish_packet> packet( new mqtt::publish_packet(msg));
					// _stored_messages.push_back(std::move(packet));
					_interface << *packet;

				}
				else
				{
					_interface << mqtt::publish_packet(msg);
				}

			}
			catch(mqtt_protocol_error& e)
			{
				// The client has run out of free message IDs.
				// For the moment: just pass the error through without disconnecting
				printf("MQTT protocol exception occurred\n");
				throw; 
			}
			catch(...)	// something went wrong with the packet transmission, which means a socket error
			{
				printf("Unhandled exception occurred while sending packet\n");
				disconnect_send();
				throw;
			}
		}
		else
			throw mqtt_protocol_error("Not connected to broker");
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::setCredentials(const std::string& strUser, const std::string& strPwd)
	{
		_mqtt_settings.setCredentials(strUser, strPwd);
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::setKeepAlive(uint16_t keep_alive_sec)
	{
		_mqtt_settings.keep_alive_time = keep_alive_sec;
	}

	// --------------------------------------------------------------------------------------------------------------------
	
	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::setWill(const std::string& topic, const std::string& msg, uint8_t qos, bool retain)
	{
		_mqtt_settings.setWill(topic, msg, qos, retain);	
	}

	// --------------------------------------------------------------------------------------------------------------------
	
	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::setHost(const std::string& hostname, uint16_t port)
	{
		if((!hostname.empty()) && (hostname != ""))
			_hostname 	= hostname;
		_port 		= port;
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
 	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::clear_queues()
	{
		clear_message_id_list();
		clear_stored_messages();
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::clear_message_id_list()
	{
		if(!isConnected())
		{
			_inflight_messag_ids = std::vector<std::pair<uint16_t, bool> >(INFLIGHT_STORAGE_DEPTH, std::make_pair(0, false));
			for(auto it = 0; it < INFLIGHT_STORAGE_DEPTH; ++it)
				_inflight_messag_ids[it].first = it;
		}
		else
			throw mqtt_protocol_error("Clearing message id list while still connected");
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::clear_stored_messages()
	{
		if(!isConnected())
		{
			_stored_messages.clear();
			// _stored_messages.reserve(INFLIGHT_STORAGE_DEPTH);
		}
		else
			throw mqtt_protocol_error("Clearing stored messages while still connected");
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	uint16_t mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::getUnusedID()
	{
		xSemaphoreTake(_id_list_mutex, portMAX_DELAY);
		for(auto it = 0; it < INFLIGHT_STORAGE_DEPTH; ++it)
			if(_inflight_messag_ids[it].second == false)
			{
				_inflight_messag_ids[it].second = true;
				printf("Assigned ID=%d to message\n", _inflight_messag_ids[it].first);
				xSemaphoreGive(_id_list_mutex);
				return _inflight_messag_ids[it].first;
			}

		xSemaphoreGive(_id_list_mutex);
		throw mqtt_protocol_error("No more free message id's");
		return 0XFFFF;
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::acknoledgePUBREC(uint16_t id)
	{
		if(isConnected())
		{
			try
			{
				_interface << mqtt::pubrel_packet(id);
			}
			catch(...)	// something went wrong with the packet transmission, which means a socket error
			{
				disconnect_send();
				throw;
			}
		}
		else
			throw mqtt_protocol_error("Not connected to broker");
	}

	// --------------------------------------------------------------------------------------------------------------------
	
	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::setID(uint16_t id, bool val)
	{
		xSemaphoreTake(_id_list_mutex, portMAX_DELAY);
		auto it = 0;
		for(it = 0; it < INFLIGHT_STORAGE_DEPTH; ++it)
			if(_inflight_messag_ids[it].first == id)
			{
				_inflight_messag_ids[it].second = val;

				if(val == false)
					printf("MessageID=%d freed\n", _inflight_messag_ids[it].first);
				
				break;
			}

		xSemaphoreGive(_id_list_mutex);
		if(it == (INFLIGHT_STORAGE_DEPTH - 1))
			throw mqtt_protocol_error("Message id was not found");
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::discardMessage(const mqtt::publish_packet& packet)
	{

		bool found = false;	
		try
		{
			for(auto it = _stored_messages.begin(); it != _stored_messages.end(); it++)
			{
				if(packet.getPacketID() == (*it)->getPacketID())
				{
					found = true;
					discardMessage((*it)->getPacketID());
					_stored_messages.erase(it);
					printf("Message with ID=%d erased\n", packet.getPacketID());
					break;
				}
			}
		}
		catch(mqtt_protocol_error& e)
		{
			// If the ID can't be unset but was found in the stored messages list, the whole stack is ill-formed
			// and action needs to be taken
			throw mqtt_protocol_error("Message-ID list and stored messages list are out of sync");
		}


		if(!found)
			throw mqtt_protocol_error("Message not found in stored messages");
	}

	// --------------------------------------------------------------------------------------------------------------------

	template<typename NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	// void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::process(const mqtt::field& packet)
	void mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>::receive()
	{
		uint16_t packet_id = 0;
		// Receive the packet from the network interface 
		mqtt::field packet;
		mqtt::field* packet_ptr = &packet;
		try
		{
			_interface >> packet;
		}
		catch(...)
		{
			printf("Network error while receiving mqtt packets\n");
			throw;
		}

		/* Determining the packet type and calling the respective (virtual) event function. */
		switch(reinterpret_cast<mqtt::fixed_header*>(packet_ptr)->getType())
		{
			// ******** Expected packets(for client) ******** 
			case mqtt::CTRL_TYPE::PUBLISH:
				onPublishReceived(*reinterpret_cast<mqtt::publish_packet*>(packet_ptr));
				// for QoS > 0 send PUBACK ?!
				break;
			case mqtt::CTRL_TYPE::PUBACK:
				discardMessage(reinterpret_cast<mqtt::puback_packet*>(packet_ptr)->getPacketID());
				break;
			case mqtt::CTRL_TYPE::PUBREC:
				/* @todo The handling of stored messages/stored IDs is not optimal and could
				 * need some rework. */
				packet_id = reinterpret_cast<mqtt::pubrec_packet*>(packet_ptr)->getPacketID();
				// try
				// { discardMessage(*reinterpret_cast<mqtt::publish_packet*>(packet_ptr)); }
				// catch(mqtt_protocol_error& e)
				// { 
				// 	std::cout << "MQTT exception occurred: " << e.what() << std::endl;
				// }
				setIDUsed(packet_id);
				acknoledgePUBREC(packet_id);
				break;
			case mqtt::CTRL_TYPE::PUBCOMP:
				discardMessage(reinterpret_cast<mqtt::pubcomp_packet*>(packet_ptr)->getPacketID());
				break;
			case mqtt::CTRL_TYPE::SUBACK:
				discardMessage(reinterpret_cast<mqtt::suback_packet*>(packet_ptr)->getPacketID());
				break;
			case mqtt::CTRL_TYPE::UNSUBACK:
				discardMessage(reinterpret_cast<mqtt::unsuback_packet*>(packet_ptr)->getPacketID());
				break;
			case mqtt::CTRL_TYPE::PINGRESP:
				_pingresp_awaited = false;
				break;
			// ******** Unexpected packets(for client) ******** 
			case mqtt::CTRL_TYPE::PUBREL:
			case mqtt::CTRL_TYPE::CONNACK: 
			case mqtt::CTRL_TYPE::CONNECT: 
			case mqtt::CTRL_TYPE::SUBSCRIBE:
			case mqtt::CTRL_TYPE::UNSUBSCRIBE:
			case mqtt::CTRL_TYPE::PINGREQ:
			case mqtt::CTRL_TYPE::DISCONNECT:
				throw protocol_exception("Unexpected control package received");					
				break;
			default:
				throw protocol_exception("Unknown control package type");					
		}
	}

}