#pragma once

#include <string>
#include <stdio.h>
#include <vector>
#include <deque>
#include <memory>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#include "mqtt_client_protocol.hpp"

namespace mqtt
{
	#define BUFFER_DEPTH 10
	/**
	 * @brief MQTT client class to be used by the application
	 * @details This class adds the thread-handling to the mqtt::mqtt_client_protocol class
	 * 
	 * @tparam NETWORK Template class for the network interface @see mqtt::dummy_interface for a sample interface
	 * @tparam INFLIGHT_STORAGE_DEPTH Buffer depth of how many messages can be stored while the client waits for a response from the broker (QoS, Subscribe requests)
	 */
	template<class NETWORK, uint32_t INFLIGHT_STORAGE_DEPTH>
	class mqtt_client : public mqtt::mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH>
	{
		typedef mqtt::mqtt_client_protocol<NETWORK, INFLIGHT_STORAGE_DEPTH> protocol;

	public:
		/**
		 * @brief Default constructor with empty client ID
		 */
		mqtt_client() : mqtt_client("") {}; 

		/**
		 * @brief Constructs an mqtt_client with an ID
		 * @details Initializes all member variables and sets the client ID
		 * 
		 * @param client_id String value, that is used in the MQTT protocol to identify the client.
		 */
		mqtt_client(const std::string& client_id) : protocol(client_id)
		{
			_send_queue 	= xQueueCreate(BUFFER_DEPTH, sizeof(uint8_t));
			_send_mutex 	= xSemaphoreCreateMutex();
			_receive_mutex 	= xSemaphoreCreateMutex();
		};

	public:	// Protocol actions
		/**
		 * @brief Connects to the broker and starts the worker threads
		 * @details Initiates the MQTT connect-procedure and upon success starts the keep-alive and receive thread.
		 */
		void connect() 
		{ 
			protocol::connect_send(); 
			if(protocol::_mqtt_settings.keep_alive_time > 0)
				xTaskCreatePinnedToCore([](void* arg)
					{	
						printf("[ ! ] Keep-alive task alive\n");
						mqtt_client* self = reinterpret_cast<mqtt_client<NETWORK, INFLIGHT_STORAGE_DEPTH>*>(arg);
						uint8_t item;
						
						while(true)
						{
							/* If there is no item send (indicated by a value in the queue), a pingreq package is send
							 * to keep the connection alive*/
							if(!xQueueReceive(self->_send_queue, &item, (1000 * self->protocol::_mqtt_settings.keep_alive_time)/portTICK_RATE_MS)) 	// keep-alive-interval
							{
								xSemaphoreTake(self->_send_mutex, portMAX_DELAY);
								printf("Sending PINGREQ\n");
								self->protocol::ping_send();
								xSemaphoreGive(self->_send_mutex);
							}
						}

						printf("[ ! ] Keep-alive task exited loop\n");
						vTaskDelete(NULL);
						self->_ping_task = nullptr;
					}, "keep_alive_task", 2048, reinterpret_cast<void*>(this), 10, &_ping_task, 0);

			xTaskCreatePinnedToCore([](void* arg)
					{	
						printf("[ ! ] Receive task alive\n");
						mqtt_client* self = reinterpret_cast<mqtt_client<NETWORK, INFLIGHT_STORAGE_DEPTH>*>(arg);
						
						while(self->_con_receiving)
						{
							xSemaphoreTake(self->_receive_mutex, portMAX_DELAY);	
							self->protocol::receive();
							xSemaphoreGive(self->_receive_mutex);
						}

						printf("[ ! ] Receive task exited loop\n");
						vTaskDelete(NULL);
						self->_receive_task = nullptr;
					}, "receive_task", 2048, reinterpret_cast<void*>(this), 10, &_receive_task, 0);
		};

		/**
		 * @brief Sends a DISCONNECT packet to the broker and closes the connection
		 * @details Stops the keep-alive and receive thread and sends a DISCONNECT packet to the broker to close the
		 * connection nicely. Afterwards the socket is closed as well.
		 */
		void disconnect() 
		{ 
			printf("Attempting a clean disconnect\n");
			xSemaphoreTake(_send_mutex, portMAX_DELAY);			// make sure the lock is not with the task we are about to kill
			printf("Sending disconnect packet\n");
			protocol::disconnect_send(); 				// send the disconnect packet
			if(_ping_task)
			{
				printf("Killing PING task\n");
				vTaskDelete(_ping_task);					// kill said task, so no more pingreq are send
				_ping_task = nullptr;
			}
			
			_con_receiving = false;				//
			vTaskDelay(100/portTICK_RATE_MS);	//
			// protocol::ping_send();				// wake receive task so the mutex is released and can be captured by this thread
			// xSemaphoreTake(_receive_mutex, portMAX_DELAY);	// make sure the lock is not with the task we are about to kill
			if(_receive_task)
			{
				printf("Killing RECEIVE task\n");
				vTaskDelete(_receive_task);						// kill said task, so no more messages are received
				_receive_task = nullptr;
			}
			// xSemaphoreGive(_receive_mutex);					// release the lock
			
			xSemaphoreGive(_send_mutex);				// release the lock
			printf("Disconnected\n");
		};

		void subscribe(const topic_list_t& topics);
		void unsubscribe(const topic_list_t& topics);

		/**
		 * @brief Publishes a message
		 * @details Publishes a message with the option of a timeout. If the timout exceeds the message is not
		 * published.
		 * 
		 * @note The method blocks until the mutex is acquired or timed out (no message is send in that case)
		 * but blocks in any case, for the time it takes to send the message over the adapter, once the mutex is acquired.
		 * 
		 * @param msg Message to be published
		 * @param timeout_ms Optional parameter that states the timeout in milliseconds. If set to 0 the method waits forever
		 * 
		 * @return Returns true if the message was published
		 */
		bool publish(const mqtt::message& msg, uint16_t timeout_ms = 0) 
		{
			uint8_t queue_val = 0;
			if(timeout_ms == 0)
			{
				xSemaphoreTake(_send_mutex, portMAX_DELAY);
				protocol::publish_send(msg);
				if(protocol::_mqtt_settings.keep_alive_time > 0)
					xQueueSend(_send_queue, &queue_val, portMAX_DELAY);
				xSemaphoreGive(_send_mutex);
				return true;
			}
			else
			{
				if(xSemaphoreTake(_send_mutex, timeout_ms/portTICK_RATE_MS))
				{
					protocol::publish_send(msg);
					if(protocol::_mqtt_settings.keep_alive_time > 0)
						xQueueSend(_send_queue, &queue_val, portMAX_DELAY);
					xSemaphoreGive(_send_mutex);
					return true;
				}
				else
					return false;
			}
		};

	protected:
		

	private:
		QueueHandle_t 		_send_queue 	= nullptr;
		SemaphoreHandle_t 	_send_mutex 	= nullptr;
		SemaphoreHandle_t 	_receive_mutex 	= nullptr;
		xTaskHandle 		_ping_task 		= nullptr;							/*!< Holds the handle to the worker thread */
		xTaskHandle 		_receive_task	= nullptr;							/*!< Holds the handle to the worker thread */
		bool 				_con_receiving 	= true;
	};
}