#pragma once

#include <string>

#include "network_client.hpp"

namespace mqtt
{
	class mqtt_network_interface : public network::network_client
	{
	public:
		inline bool isOpen() const { return network::network_client::isOK(); };

		template<typename T>
		inline void operator<<(const T& packet) { network::network_client::send(packet); };

		template<typename T>
		inline void operator>>(T& rhs) 
		{ 
			mqtt_network_interface::receive(); 
			rhs = static_cast<T>(_incoming_pkg);
		};

	private: 
		mqtt::fixed_header _incoming_pkg;

		void receive()
		{
			if(network::network_client::isOK())
			{
				// struct timeval receiving_timeout;
				// receiving_timeout.tv_sec = 5;
				// receiving_timeout.tv_usec = 0;
				// setsockopt(_sockfd, SOL_SOCKET, SO_RCVTIMEO, &receiving_timeout, sizeof(receiving_timeout));


				// printf("======= Receiving =======\n");
				ssize_t recv_bytes = 0;
				ssize_t recv_bytes_total = 0;
				uint8_t* buffer = new uint8_t;
				mqtt::field header;
				header.reserve(5);

				// *************************************
				// Determine the incoming packet type
				for(auto it = 0; it < 5; ++it)
				{
					recv_bytes = recv(_sockfd, buffer, 1, 0);
					if(recv_bytes < 0)
					{
						if(errno != 11)
						{
							printf("Error while receiving packet header errno=%d\n", errno);
							printf("%s\n", strerror(errno));
							header.print();
							network::network_client::close();
							throw network::network_error("Failed to receive packet", header);
						}
						else
							throw mqtt::protocol_exception("Socket timed out during receive");		
					}

					header.push_back(*buffer);
					if((it > 0) && ((*buffer & 0x80) == 0))	// remaining length has no more fields
					{
						// printf("header complete after %d element(s)\n", it);
						break;
					}
				}

				// header.print();
				
				// integrity check
				_incoming_pkg = mqtt::fixed_header(header);
				try
				{
					_incoming_pkg.checkIntegrity();
					std::cout << "Got packet: " << _incoming_pkg.getType() << std::endl;
				}
				catch(mqtt::protocol_exception& e)
				{
					printf("Error while checking integrity of the packet:\n");
					_incoming_pkg.print();
					printf("%s", e.what());
					network::network_client::close();
					throw;
				}

				// *************************************
				// Get the remaining bytes if there are any
				// printf("----\n");
				recv_bytes_total = 0;
				uint32_t remaining_len = _incoming_pkg.getRemainingLength();
				delete buffer;
				if(remaining_len > 0)
				{
					buffer = new uint8_t[remaining_len];
				
					while((recv_bytes_total < remaining_len) && (recv_bytes > 0))
					{
						recv_bytes = recv(_sockfd, buffer + recv_bytes_total, remaining_len - recv_bytes_total, 0);
						if(recv_bytes < 0)
						{
							printf("Error while receiving packet body\n");
							for(auto it = 0; it < recv_bytes_total; ++it)
								printf("%#02x  ", buffer[it]);
							printf("\n");
							network::network_client::close();
							throw network::network_error("Failed to receive packet");
						}
						recv_bytes_total += recv_bytes;
					}
					// printf("Got %d more bytes (should be %d)\n", recv_bytes_total, remaining_len);
						
					// move bytes to vector (mqtt::field)
					for(auto it = 0; it < remaining_len; ++it)
						_incoming_pkg.push_back(buffer[it]);	

					delete[] buffer;			
				}

				// _incoming_pkg.print();
				// printf("======= Receiving DONE =======\n");

			}
			else
				throw network::network_error("Receive: Socket not open");

		}


	};
}