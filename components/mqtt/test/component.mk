COMPONENT_ADD_LDFLAGS = -Wl,--whole-archive -l$(COMPONENT_NAME) -Wl,--no-whole-archive
COMPONENT_ADD_INCLUDEDIRS := ../protocol/inc ../client/inc
COMPONENT_SRCDIRS := ../protocol/src ../client/src
CXXFLAGS += -std=c++14 -fexceptions
CPPFLAGS += -std=c++14 -fexceptions