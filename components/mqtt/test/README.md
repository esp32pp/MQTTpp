# MQTT++ Unit and Integration Tests

This folder holds unit tests as well as integrations tests. To assure MQTT 3.1.1 conformity as well as robust behaviour, automated scripts are used.

**The decision on the framework of choice is not yet completed**

## List of Unit Test Framework Candidates
* [Google Test](https://github.com/google/googletest)
* [UnitTest++](https://github.com/unittest-cpp/unittest-cpp/wiki)
* ESP-IDF Unity (*Does not recognize test cases. No idea why*)