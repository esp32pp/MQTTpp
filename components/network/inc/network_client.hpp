#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <exception>

// #include "freertos/task.h"
#include "lwip/sockets.h"
#include "lwip/netdb.h"

namespace network
{
	class network_error : std::exception
	{
	public:
		
					
		network_error(const std::string& what) : _what(what) {};

		network_error(const std::string& what, const std::vector<uint8_t>& packet) : _what(what) 
		{
			buffer_ptr = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(packet.data()));
			buffer_len = packet.size();
			buffer.insert(buffer.begin(), packet.begin(), packet.end());
		};	
		
		const char* what() const noexcept { return _what.c_str(); }
		
		std::vector<uint8_t> buffer;	
		uint8_t* buffer_ptr = nullptr;	
		uint32_t buffer_len = 0;	

	private:
		std::string _what;

	};

	enum class STATE : uint8_t
	{
		OFFLINE 		= 0,
		CONNECTING 		= 1,
		CONNECTED 		= 2,
		DISCONNECTING 	= 3
	};

	class network_client
	{
	public:
		network_client()
		{
			_socket_state	= STATE::OFFLINE;
			_sockfd 		= -1;
			memset(&_sock_addr, 0, sizeof(_sock_addr));
		};
		~network_client() { network_client::close(); }
		
		void open(const std::string& host, uint16_t port)
		{
			if(isOK())
			{
				network_client::close();
			}
			
			_socket_state = STATE::CONNECTING;

			_hp = gethostbyname(host.c_str());
			if (!_hp) 
			{
				printf("Resolving hostname '%s' failed (errno=%d)\n", host.c_str(), errno);
				printf("%s\n", strerror(errno));
				_socket_state = STATE::OFFLINE;
				throw network_error("Resolving hostname failed");
			}

			_ip4_addr = (struct ip4_addr *)_hp->h_addr;

			_sockfd = socket(AF_INET, SOCK_STREAM, 0);
			if (_sockfd < 0) 
			{
				printf("Creating socket failed (errno=%d)\n", errno);
				printf("%s\n", strerror(errno));
				_socket_state = STATE::OFFLINE;
				throw network_error("Creating socket failed");
			}

			memset(&_sock_addr, 0, sizeof(_sock_addr));
			_sock_addr.sin_family 		= AF_INET;
			_sock_addr.sin_addr.s_addr 	= 0;
			_sock_addr.sin_port 		= htons(port);
			int ret = bind(_sockfd, (struct sockaddr*)&_sock_addr, sizeof(_sock_addr));
			if (ret) 
			{
				printf("Binding socket to port %d failed (errno=%d)\n", port, errno);
				printf("%s\n", strerror(errno));
				_sockfd = -1;
				_socket_state = STATE::OFFLINE;
				throw network_error("Binding socket failed");
			}

			memset(&_sock_addr, 0, sizeof(_sock_addr));
			_sock_addr.sin_family 		= AF_INET;
			_sock_addr.sin_addr.s_addr 	= _ip4_addr->addr;
			_sock_addr.sin_port 		= htons(port);
			ret = connect(_sockfd, (struct sockaddr*)&_sock_addr, sizeof(_sock_addr));
			if (ret) 
			{
				printf("Connecting to host failed (errno=%d)\n", errno);
				printf("%s\n", strerror(errno));
				_sockfd = -1;
				_socket_state = STATE::OFFLINE;
				throw network_error("Connecting to host failed");
			}
			_socket_state = STATE::CONNECTED;
			printf("Connected to %s@%d\n", host.c_str(), port);
			
		}

		void close()
		{
			if(isOK())
			{
				_socket_state = STATE::DISCONNECTING;
				::close(_sockfd);
				_socket_state = STATE::OFFLINE;
			}
		}

		inline bool isOK() const { return ((_socket_state == STATE::CONNECTED) && (_sockfd > 0)); }

		void send(const std::vector<uint8_t>& packet)
		{
			if(isOK())
			{
				// printf("======= Sending =======\n");
				// printf("DEBUG (SEND): packet length=%dbytes\n", packet.size());
				// static_cast<mqtt::field>(packet).print();

				send_bytes = 0;
				send_bytes_total = 0;
				
				const uint8_t* buffer = reinterpret_cast<const uint8_t*>(packet.data());
				do
				{
					send_bytes = ::send(_sockfd, buffer + send_bytes_total, packet.size() - send_bytes_total, 0);
					if(send_bytes < 0)
					{
						printf("Error while sending packet\n");
						network_client::close();
						// throw network::network_error("Failed to send packet");
						throw network::network_error("Failed to send packet", packet);
					}
					send_bytes_total += send_bytes;
				}while(send_bytes_total < packet.size());
				// printf("======= Sending DONE =======\n");
			}
			else
				throw network::network_error("Send: Socket not open");
		}

		

	protected:
		int 				_sockfd;
		struct sockaddr_in 	_sock_addr;
		struct hostent* 	_hp;
		struct ip4_addr* 	_ip4_addr;

		ssize_t send_bytes = 0;
		ssize_t send_bytes_total = 0;

		STATE _socket_state;
	};
}