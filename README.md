# MQTT++ for ESP32

This library implements the MQTT 3.1.1 standard as C++ library based on the STL with a `std::vector<uint8_t>` as base type for all packets. It's target application are embedded devices but the core should be usable also on other environments that support the STL.

**The implementation is not yet complete!**

## Featurelist

| Feature | Implementation Status | Comments |
| ------- | --------------------- | -------- |
| Connect to broker | :white_check_mark: | |
| Send QoS 0 packets | :white_check_mark: | |
| Keep Alive via PINGREQ | :white_check_mark: | |
| Send Qos >0 packets | :white_check_mark: | Not tested |
| Subscribe/Unsubscribe | |
| Subscribe to QoS >0 | |
| Encryption (e.g. TLS) | |
| WebSocket support | |

## Protocol
The protocol (files in `protocol` subdirectory) define the packets of the [MQTT 3.1.1 standard](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/csprd02/mqtt-v3.1.1-csprd02.html). 
So far **all packets are implemented**, but verifivcation has only been carried out for the
* connect
* connack
* publish
* pingreq
* pingresp
* disconnect
packets.

### Known Issues
The `set()` method could possibly lead to ill-formed packets, if called more then once as the exisiting field is not cleared/deleted before new fields are appended.

Due to the `std::vector` as base, the creation of packets is not the most performant. It was chosen non the less because of it's clean code structure and portability.

Publishing data packets with more then `100` bytes payload the socket crashes while sending it. Maybe that has to do with the limited memory of the MAC/IP stack (menuconfig->component->ethernet  & lwip)

## Client
A client is implemented (files in the `client` subdirecotry) in a 2-stage class hirarchy. The first implements the protocol (which packt has to be sent) while the 2nd class is derived from this one and implements multithreading/timing of send and receive.

To use the client, a application-specific child class has to be derived from the 2nd stage class to handle the `onPublishReceived` method, that is called when a message arrives from a subscribed topic. **This part is still subject to change**

## Server
Not planned at this point

The ESP32 might though be able to act as a broker with a limited number of connections. It should be also possible to use the ESP32 as a bridge between MQTT over TCP/WiFi to a e.g. serial bus (e.g. via MQTT-SN).